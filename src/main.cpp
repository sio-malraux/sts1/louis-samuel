#include <libpq-fe.h>
#include <iostream>
#include <cstring>
#include <iomanip>

enum CODES_ERREUR
{
  OK_TOUT_VA_BIEN,
  ERR_NO_PING,
  ERR_NO_CONNECTION,
};

int main()
{
  int code_retour;
  char information_de_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=l.barbier user=l.barbier password=P@ssword";
  PGPing code_retour_ping;
  PGconn *connexion;
  code_retour = OK_TOUT_VA_BIEN;
  code_retour_ping = PQping(information_de_connexion);
  int nb_caracteres;
  const char *requete_sql = "SELECT * FROM si6.\"Animal\" JOIN si6.\"Race\" on si6.\"Animal\".race_id = si6.\"Race\".id WHERE si6.\"Animal\".sexe = 'Femelle' AND si6.\"Race\".nom = 'Singapura';";
  int nbColonnes, nbLignes, taille;

  if(code_retour_ping == PQPING_OK)
  {
    // etablissement des connexions ou affichage des erreurs
    std::cout << "le serveur est accessible" << std::endl;
    connexion = PQconnectdb(information_de_connexion);

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      std::cout << "la connexion est établie" << std::endl;
      std::cout << "La connexion est établie" << std::endl;
      std::cout << "La connexion au serveur de base de données de " << PQhost(connexion) << " a bien été établie avec les paramètres suivants :" << std::endl;
      std::cout << " * utilisateur : " << PQuser(connexion) << std::endl;
      std::cout << " * mot de passe : ";
      nb_caracteres = sizeof PQpass(connexion);

      for(int i = 0; i < nb_caracteres; ++i)
      {
        std::cout << '*';
      }

      std::cout << " (<-- autant de '*' que la longueur du mot de passe)" << std::endl;
      std::cout << " * base de données : " << PQdb(connexion) << std::endl;
      std::cout << " * port TCP : " << PQport(connexion) << std::endl;
      std::cout << std::boolalpha << " * chiffrement SSL : " << (bool)(PQgetssl(connexion) != 0) << std::endl;
      std::cout << " * encodage : " << pg_encoding_to_char(PQclientEncoding(connexion)) << std::endl;
      std::cout << " * version du protocole : " << PQprotocolVersion(connexion) << std::endl;
      std::cout << " * version du serveur : " << PQserverVersion(connexion) << std::endl;
      std::cout << " * version de la bibliothèque 'libpq' du client : " << PQlibVersion() << std::endl;
    }

    const char requete_schema[] = "SET schema 'si6';  SELECT * FROM \"Animal\" INNER JOIN \"Race\" ON \"Animal\".race_id = \"Race\".id WHERE sexe = 'Femelle' AND \"Race\".nom = 'Singapura';";
    PGresult *execution = PQexec(connexion, requete_schema);
    ExecStatusType resultat = PQresultStatus(execution);

    if(resultat == PGRES_EMPTY_QUERY)
    {
      std::cerr << "La chaîne envoyée était vide.";
    }
    else if(resultat == PGRES_COMMAND_OK)
    {
      std::cerr << "Fin avec succès d'une commande ne renvoyant aucune donnée.";
    }
    else if(resultat == PGRES_TUPLES_OK)
    {
      nbColonnes = PQnfields(execution);
      int nbEnregistrements = PQntuples(execution);
      int tiret = 0;
      nbColonnes = PQnfields(execution);
      nbLignes = PQntuples(execution);

      for(int ligne = 0; ligne < nbLignes; ligne++)
      {
        for(int col = 0; col < nbColonnes; col++)
        {
          int titre = std::strlen(PQfname(execution, col));

          if(titre > taille)
          {
            taille = titre;
          }

          int length = PQgetlength(execution, ligne, col);

          if(length < 30 && length > taille)
          {
            taille = length;
          }
        }
      }
      for(int i = 0; i < nbColonnes; i++)
      {
        std::setiosflags(std::ios::left);
        std::cout << std::left << std::setw(taille) << PQfname(execution, i) << "|";
      }

	 for(int j = 0; j < nbLignes; j++)
        {
          for(int k = 0; k < nbColonnes; k++)
          {
            std::cout << std::setw(taille) << PQgetvalue(execution, j, k) << "|";
          }
      }

      std::cout << std::endl << "L'exécution de la requête SQL a retourné " << nbEnregistrements << " enregistrements." << std::endl;
      PQclear(execution);
    }
    else if(resultat == PGRES_COPY_OUT)
    {
      std::cerr << "Début de l'envoi (à partir du serveur) d'un flux de données.";
    }
    else if(resultat == PGRES_COPY_IN)
    {
      std::cerr << "Début de la réception (sur le serveur) d'un flux de données.";
    }
    else if(resultat == PGRES_BAD_RESPONSE)
    {
      std::cerr << "La réponse du serveur n'a pas été comprise.";
    }
    else if(resultat == PGRES_NONFATAL_ERROR)
    {
      std::cerr << "Une erreur non fatale (une note ou un avertissement) est survenue.";
    }
    else if(resultat == PGRES_FATAL_ERROR)
    {
      std::cerr << "Une erreur fatale est survenue.";
    }
    else
    {
      std::cerr << "Erreur de connexion" << std::endl;
    }
  }
  else
  {
    std::cerr << "le serveur n’est pas accessible pour le moment, veuillez contacter votre administrateur." << std::endl;
    code_retour = ERR_NO_PING;
  }

  return code_retour;
}
